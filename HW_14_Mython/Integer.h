#ifndef INTEGER_H
#define INTEGER_H
#include "type.h"

class Integer :public Type
{
private:
	int data;
public:
	Integer(int data);
	virtual bool isPrintable() const;
	virtual string toString() const;


};

#endif // INTEGER_H