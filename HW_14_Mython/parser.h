#ifndef PARSER_H
#define PARSER_H

#include "InterperterException.h"
#include "type.h"
#include "Helper.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>

using namespace std;
static unordered_map<string, Type*> _variables;
class Parser
{
public:
	static Type* parseString(std::string str) throw();
	static Type* getType(std::string& str);

private:
	static bool isLegalVarName(const std::string& str);
	static bool makeAssignment(const std::string& str);
	static Type* getVariableValue(const std::string& str);
		
};

#endif //PARSER_H
