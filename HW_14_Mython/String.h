#ifndef STRING_H
#define STRING_H
#include "type.h"
#include "Sequence.h"
#include <iostream>

using namespace std;

class String :public Sequence
{
private:
	string data;
public:
	String(string data);
	virtual bool isPrintable() const;
	virtual string toString() const;
};

#endif // STRING_H