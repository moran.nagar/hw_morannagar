#include "NameErrorException.h"


const char* NameErrorException::what() const throw()
{
	return "NameError: this name is not defined";
}
