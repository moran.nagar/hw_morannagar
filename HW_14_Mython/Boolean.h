#ifndef BOOLEAN_H
#define BOOLEAN_H
#include "type.h"
#include <iostream>
#include "Helper.h"

class Boolean :public Type
{
private:
	bool data;
public:
	Boolean(bool data);
	virtual bool isPrintable() const;
	virtual string toString() const;


};

#endif // BOOLEAN_H