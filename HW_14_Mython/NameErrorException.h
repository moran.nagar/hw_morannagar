#ifndef NAME_ERROR_EXCEPTION_H
#define NAME_ERROR_EXCEPTION_H
#include "InterperterException.h"
#include <iostream>

using namespace std;
class NameErrorException : public InterperterException
{
private:
	const char* _name;
public:
	virtual const char* what() const throw();
};


#endif // NAME_ERROR_EXCEPTION_H