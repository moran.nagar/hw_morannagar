#include "type.h"
#include "InterperterException.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include "NameErrorException.h"
#include "parser.h"
#include <iostream>

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "Moran"

using namespace std;

int main(int argc,char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;
	Type* input_type;
	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	
	while (input_string != "quit()")
	{
		try
		{
			input_type = Parser::parseString(input_string);
			if (input_type != NULL && input_type->isPrintable())
			{
				cout << input_type->toString() << endl;
				if (input_type->get_isTemp())
				{
					delete input_type;
				}
			}
		}
		catch (InterperterException &ex)
		{
			printf("%s\n",ex.what() );
		}
		

		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	return 0;
}


