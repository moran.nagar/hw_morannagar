#ifndef TYPE_H
#define TYPE_H
#include <iostream>

using namespace std;

class Type
{
private:
	bool _isTemp = false;
public:
	bool get_isTemp()const;
	void set_isTemp(const bool isTemp);
	virtual bool isPrintable() const = 0;
	virtual string toString() const = 0;
};





#endif //TYPE_H
