#include "parser.h"
#include "IndentationException.h"
#include "NameErrorException.h"
#include "SyntaxException.h"
#include "Helper.h"
#include "Integer.h"
#include "Boolean.h"
#include "String.h"
#include "Void.h"
#include <iostream>

using namespace std;

Type* Parser::parseString(std::string str) throw()
{
	Type* t = NULL;
	if (str.length() > 0)
	{
		if (str[0] == ' ' || str[0] == '\t')
		{
			throw IndentationException();
		}
		Helper::rtrim(str);
		bool ans = makeAssignment(str);
		if (!ans)
		{
			Type* val = getVariableValue(str);
			if (val == NULL)
			{
				if (!Parser::isLegalVarName(str))
				{
					t = getType(str);
				}
				else
				{
					throw NameErrorException();
				}
			}
			else
			{
				cout << val->toString() << endl;
			}
		}
		else
		{	
			t = new Void();
		}	
		
	}

	return t;
}

Type* Parser::getType(std::string& str)
{
	Type *t = NULL;
	if (Helper::isInteger(str))
	{
		t = new Integer(stoi(str));
	}
	else if (Helper::isBoolean(str))
	{
		if (str == "True")
		{
			t = new Boolean(true);
		}
		else
		{
			t = new Boolean(false);
		}	
	}
	else if (Helper::isString(str))
	{
		t = new String(str);
	}
	else
	{
		throw SyntaxException();
	}
	return t;
}

bool Parser::isLegalVarName(const std::string& str)
{
	if (Helper::isDigit(str[0]))
	{
		return false;
	}
	for (int i = 1; i < str.length(); i++)
	{
		if (!((Helper::isLetter(str[i])||(Helper::isDigit(str[i]))|| (Helper::isUnderscore(str[i])))))
		{
			return false;
		}
	}
	return true;
}

bool Parser::makeAssignment(const std::string& str)
{
	std::size_t found = str.find('=');
	if (found != std::string::npos)
	{
		string name = str.substr(0, found);
		string value = str.substr(found + 1, str.length());
		Helper::trim(name);
		Helper::trim(value);
		if (!isLegalVarName(name))
		{
			throw NameErrorException();
		}
		Type* value_type = getType(value);
		_variables[name] = value_type;
		return true;
	}
	return false;
}

Type* Parser::getVariableValue(const std::string& str)
{
	std::unordered_map<std::string, Type*>::const_iterator got = _variables.find(str);
	if (got == _variables.end())
	{
		return NULL;
	}
	return got->second;
}















