#include "Integer.h"
#include "Helper.h"

Integer::Integer(int data)
{
	this->data = data;
}
bool Integer::isPrintable() const
{
	return true;
}
string Integer::toString() const
{
	return to_string(this->data);
}