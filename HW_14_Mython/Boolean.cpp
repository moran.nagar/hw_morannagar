#include "Boolean.h"

using namespace std;

Boolean::Boolean(bool data)
{
	this->data = data;
}
bool Boolean::isPrintable() const
{
	return true;
}
string Boolean::toString() const
{
	if (this->data)
	{
		return "True";
	}
	return "False";
}
