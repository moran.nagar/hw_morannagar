#include "Arrow.h"

void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}
Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name) :Shape(name, type)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
}
Arrow::~Arrow()
{

}
double Arrow::getArea() const
{
	return 0;
}
double Arrow::getPerimeter() const
{
	return this->_points[0].distance(this->_points[1]);
}
