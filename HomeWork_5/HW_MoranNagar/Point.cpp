#include "Point.h"
#include <cmath>

Point::Point(double x, double y)
{
	this->x = x;
	this->y = y;
}
Point::Point(const Point& other)
{
	*this = other;
}
Point::~Point() 
{

}

Point Point::operator+(const Point& other) const
{
	return Point(0, 0);
}
Point& Point::operator+=(const Point& other)
{
	this->x += other.x;
	this->y += other.y;
	return *this;
}

double Point::getX() const
{
	return this->x;
}
double Point::getY() const
{
	return this->y;
}

double Point::distance(const Point& other) const
{
	if (this->x == other.x)
		return abs(this->y - other.y); 
	return abs(this->x - other.x);
}