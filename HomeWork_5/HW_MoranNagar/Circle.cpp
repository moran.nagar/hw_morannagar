#include "Circle.h"

Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) :Shape(name, type)
{
	this->Center = center;
	this->Radius = radius;
}
Circle::~Circle()
{

}

const Point& Circle::getCenter() const
{
	return this->Center;
}
double Circle::getRadius() const
{
	return this->Radius;
}
void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}


