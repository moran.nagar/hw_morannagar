#pragma once
#include "Shape.h"
#include "Canvas.h"
#include "Circle.h"
#include "Arrow.h"
#include "Rectangle.h"
#include "Triangle.h"
#include <vector>



class Menu
{
public:

	Menu();
	~Menu();

	void optionZero()const;

private: 
	Canvas _canvas;
};

