#include "Triangle.h"


Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name):Polygon(type,name)
{
	this->_points[0] = a;
	this->_points[1] = b;
	this->_points[2] = c;
}
Triangle::~Triangle()
{

}
double Triangle::getArea() const
{
	int z = this->_points[1].distance(this->_points[2]);
	int height = this->_points[0].distance(Point(this->_points[0].getX(), this->_points[1].getY()));
	if (z > 0 && height > 0)
	{
		return 0.5 * height * z;
	}
	std::cout << "Error: One of the points are wrong";
	return 0;
}
double Triangle::getPerimeter() const
{
	int x = this->_points[0].distance(this->_points[1]);
	int y = this->_points[0].distance(this->_points[2]);
	int z = this->_points[1].distance(this->_points[2]);
	if (x > 0 && y > 0 && z > 0)
	{
		return x + y + z;
	}
	std::cout << "Error: One of the points are wrong";
	return 0;
}
void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}
