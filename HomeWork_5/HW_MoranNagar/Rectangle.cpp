#include "Rectangle.h"

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name):Polygon(type, name)
{
	if (width > 0 && length > 0)
	{
		this->_points[0] = a;
		Point* p = new Point(a.getX() + width, a.getY() + length);
		this->_points[1] = *p;
	}
}

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}
double myShapes::Rectangle::getArea() const
{
	Point* p1 = new Point(this->_points[0].getX(), this->_points[1].getY());//Down Left
	Point* p2 = new Point(this->_points[1].getX(), this->_points[0].getY());//Top Right
	int length = p1->distance(this->_points[0]);
	int width = p2->distance(this->_points[0]);
	if (length > 0 && width > 0)
		return length * width;
}
double myShapes::Rectangle::getPerimeter() const
{
	Point* p1 = new Point(this->_points[0].getX(), this->_points[1].getY());//Down Left
	Point* p2 = new Point(this->_points[1].getX(), this->_points[0].getY());//Top Right
	int length = p1->distance(this->_points[0]);
	int width = p2->distance(this->_points[0]);
	if (length > 0 && width > 0)
		return (length * 2) + (width * 2);
}

