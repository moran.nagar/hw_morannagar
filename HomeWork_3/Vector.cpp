#include <iostream>
#include "Vector.h"

Vector::Vector(int n)
{
	this->_size = 0;
	if (n < 2)
	{
		n = 2;
	}
	this->_capacity = n;
	this->_elements = new int[n]();
	this->_resizeFactor = n;
}

Vector::~Vector()
{
	delete[] this->_elements;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}
int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}
bool Vector::empty() const
{
	return this->_size == 0;
}


void Vector::push_back(const int& val)
{
	if (this->_size == this->_capacity)
	{
		this->add_places(this->_capacity + this->_resizeFactor);
	}
	this->_elements[this->_size] = val;
	this->_size++;
}

void Vector::add_places(int n)
{
	int* oldArr = this->_elements;
	this->_elements = new int[n]();
	for (int i = 0; i < this->_capacity; i++)
	{
		this->_elements[i] = oldArr[i];
	}
	delete[] oldArr;
	this->_capacity = n;
}

void Vector::deleting_places(int n)
{
	int* oldArr = this->_elements;
	this->_elements = new int[n]();
	for (int i = 0; i < n ; i++)
	{
		this->_elements[i] = oldArr[i];
	}
	delete[] oldArr;
	this->_capacity = n;
}

int Vector::pop_back()
{
	int num = -9999;
	if (this->_size > 0)
	{
		num = this->_elements[this->_size - 1];
		this->_elements[this->_size - 1] = 0;
		this->_size--;
	}
	else
	{
		std::cout << "error: pop from empty vector\n";
	}
	return num;
}
void Vector::reserve(int n)
{
	if (this->_capacity < n)
	{
		int num = n - this->_capacity;
		if (num % this->_resizeFactor != 0)
		{
			num = (num / this->_resizeFactor) + 1;
		}
		else
		{
			num = num / this->_resizeFactor;
		}
		this->add_places(num * this->_resizeFactor + this->_capacity);
	}
}
void Vector::resize(int n)
{
	if (n < this->_capacity)
	{
		this->deleting_places(n);
	}
	else if (n > this->_capacity)
	{
		this->reserve(n);
	}
}
void Vector::assign(int val)
{
	for (int i = 0; i < this->_size - 1; i++)
	{
		this->_elements[i] = val;
	}
}
void Vector::resize(int n, const int& val)
{
	
	if (n < this->_capacity)
	{
		this->deleting_places(n);
		if (this->_size > n)
		{
			this->_size = n;
		}
	}
	else if (n > this->_capacity)
	{
		this->reserve(n);
		for (int i = this->_capacity; i < n; i++)
		{
			this->_elements[i] = val;
			this->_size++;
		}
	}
}

Vector::Vector(const Vector& other)
{
	*this = other;
}
Vector& Vector::operator=(const Vector& other)
{
	if (this == &other) 
	{
		return *this;
	}

	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;


	this->_elements = new int[this->_capacity];
	for (int i = 0; i < this->_capacity; i++)
	{
		this->_elements[i] = other._elements[i];
	}

	return *this;
}


int& Vector::operator[](int n) const
{
	int& val = this->_elements[0];
	if (n >= this->_capacity)
	{
		std::cout << "error: There is no such place in the array\n";
	}
	else
	{
		val = this->_elements[n];
	}
	return val;
}