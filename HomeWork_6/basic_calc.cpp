#include <iostream>
const int THE_FORBIDDEN_NUMBER = 8200;

void add(int a, int b, int& result) {
	if ((a + b) == THE_FORBIDDEN_NUMBER)
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n";
	else
		result = a + b;
}

void  multiply(int a, int b, int& result) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		if (sum == THE_FORBIDDEN_NUMBER)
			std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n";
		else
		{
			result = sum;
			add(sum, a, sum);
		}
	};
	result = sum;
}

void  pow(int a, int b, int& result) 
{
  int exponent = 1;
  for(int i = 0; i < b; i++) {
	  if (exponent == THE_FORBIDDEN_NUMBER)
		  std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n";
	  else
	  {
		  result = exponent;
		  multiply(exponent, a, exponent);
	  }
  };
  result = exponent;
}

int main(void) 
{
	int result = 0;
	pow(8200, 1, result);
	add(1, 8199, result);
	multiply(4100, 2, result);
	std::cout << "\n\n\n";

	pow(5, 5, result);
	std::cout << result;
	std::cout << "\n";
	add(5, 5, result);
	std::cout << result;
	std::cout << "\n";
	multiply(5, 5, result);
	std::cout << result;
	std::cout << "\n";


}