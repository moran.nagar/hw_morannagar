#include <iostream>
const int THE_FORBIDDEN_NUMBER = 8200;

int add(int a, int b) {
	if ((a + b) == THE_FORBIDDEN_NUMBER)
		throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n"));
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		if (sum == THE_FORBIDDEN_NUMBER)
			throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n"));
		sum = add(sum, a);
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		if (exponent == THE_FORBIDDEN_NUMBER)
			throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n"));
		exponent = multiply(exponent, a);
	};
	return exponent;
}

int main(void) {
	try
	{
		std::cout << pow(8200, 1) << std::endl;
	}
	catch (const std::string & errorString)
	{
		std::cerr << "ERROR: " << errorString;
	}

	try
	{
		std::cout << add(8199, 1) << std::endl;
	}
	catch (const std::string & errorString)
	{
		std::cerr << "ERROR: " << errorString;
	}

	try
	{
		std::cout << multiply(4100, 2) << std::endl;
	}
	catch (const std::string & errorString)
	{
		std::cerr << "ERROR: " << errorString;
	}
}