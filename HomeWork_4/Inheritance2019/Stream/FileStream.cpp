#define _CRT_SECURE_NO_WARNINGS
#include "FileStream.h"


FileStream::FileStream(char* name)
{
	this->name = name;
	this->file = fopen(name, "w");
	if (this->file == NULL)
	{
		char* error = "Can't open this file but the message is: ";
		this->file = stdout;
		*this << error;
	}
}
FileStream::~FileStream()
{
	fclose(this->file);
}