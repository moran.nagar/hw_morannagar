#pragma once
#include "OutStream.h"
class OutStreamEncrypted : public OutStream
{
private:
	int code;
public:
	OutStreamEncrypted& operator<<(const char* str);
	OutStreamEncrypted(int code);
};