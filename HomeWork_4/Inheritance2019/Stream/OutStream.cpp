#include "OutStream.h"
#include <stdio.h>

OutStream::OutStream()
{
	this->file = stdout;
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(this->file,"%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(this->file,"%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)())
{
	pf();
	return *this;
}


void endline()
{
	printf("\n");
}
