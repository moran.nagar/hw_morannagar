#pragma once
#include "OutStream.h"
#define _CRT_SECURE_NO_WARNINGS

class FileStream : public OutStream
{
public:
	FileStream(char* name);
	~FileStream();
private:
	char *name;
};