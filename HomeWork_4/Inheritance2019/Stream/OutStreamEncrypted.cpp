#include "OutStreamEncrypted.h"
#include <string.h>
#define FIRST_OPTION 32
#define LAST_OPTION 126

OutStreamEncrypted::OutStreamEncrypted(int code)
{
	this->code = code;
}
OutStreamEncrypted& OutStreamEncrypted::operator<<(const char* str)
{
	char* str_encrypted = new char;
	for (int i = 0; i < strlen(str); i++)
	{
		if ((FIRST_OPTION <= str[i]) && (str[i] <= LAST_OPTION))
		{
			str_encrypted[i] = char(int(str[i]) + this->code);
		}
	}
	fprintf(this->file, "%s", str_encrypted);
	return *this;
}


