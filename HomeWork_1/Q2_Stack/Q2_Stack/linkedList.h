#pragma once

typedef struct linkedList
{
	int value;
	struct linkedList* next;
} linkedList;


linkedList* add(linkedList* list, unsigned int newValue);
int remove(linkedList* list); 
