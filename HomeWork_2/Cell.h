#pragma once
#include "Ribosome.h"
#include "Nucleus.h"
#include <iostream>
#include <string>
#include "Mitochondrion.h"


class Cell
{
private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocuse_receptor_gene;
	unsigned int _atp_units;
public:
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	bool get_ATP();

};