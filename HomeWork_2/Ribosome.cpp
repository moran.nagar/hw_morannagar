#include <iostream>
#include <string>
#include "Protein.h"
#include "Ribosome.h"
#include "AminoAcid.h"

Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	Protein* protein = new Protein;
	protein->init();
	int len = RNA_transcript.length();
	AminoAcid* amino = NULL;
	std::string temp = "";
	while (len >= 3)
	{
		temp = RNA_transcript.substr(0, 3);
		amino = new AminoAcid;
		*amino = get_amino_acid(temp);
		if (*amino == UNKNOWN)
		{
			protein->clear();
			return nullptr;
		}
		else
		{
			protein->add(*amino);
		}
		len = RNA_transcript.length() - 3;
		RNA_transcript = RNA_transcript.substr(3, RNA_transcript.length());
	}
	return protein;
}