
#include "Ribosome.h"
#include "Nucleus.h"
#include <iostream>
#include <string>
#include "Mitochondrion.h"




void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	AminoAcidNode* p1 = protein.get_first();
	AminoAcidNode* p2 = make_glocuse()->get_first();
	bool flag = true;
	while (flag && p1 != NULL && p2 != NULL)
	{
		if (p1->get_data() != p2->get_data())
		{
			flag = false;
		}
		else
		{
			p1 = p1->get_next();
			p2 = p2->get_next();
		}
	}
	if ((p1 != NULL && p2 == NULL) || (p2 != NULL && p1 == NULL))
	{
		flag = false;
	}
	this->_has_glocuse_receptor = flag;
}

Protein* Mitochondrion::make_glocuse() const
{
	Protein* p = new Protein();
	p->add(ALANINE);
	p->add(LEUCINE);
	p->add(GLYCINE);
	p->add(HISTIDINE);
	p->add(LEUCINE);
	p->add(PHENYLALANINE);
	p->add(AMINO_CHAIN_END);
	return p;
}


void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP() const
{
	return this->_has_glocuse_receptor && this->_glocuse_level >= 50;
}