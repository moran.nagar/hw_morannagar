#include "Ribosome.h"
#include "Nucleus.h"
#include <iostream>
#include <string>
#include "Mitochondrion.h"
#include "Cell.h"

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();
	this->_glocuse_receptor_gene = glucose_receptor_gene;
}
bool Cell::get_ATP()
{
	std::string temp = this->_nucleus.get_RNA_transcript(this->_glocuse_receptor_gene);
	Protein* protein = this->_ribosome.create_protein(temp);
	if (protein == nullptr)
	{
		std::cerr << "dna sequence is not valid" << std::endl;
		_exit(1);
	}
	this->_mitochondrion.insert_glucose_receptor(*protein);
	this->_mitochondrion.set_glucose(50);
	if (!this->_mitochondrion.produceATP())
	{
		return false;
	}
	this->_atp_units = 100;
	return true;
}