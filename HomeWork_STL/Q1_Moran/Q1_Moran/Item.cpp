#include "Item.h"

using namespace std;

Item::Item(std::string name, std::string serialNumber, double unitPrice, int count)
{
	this->_name = name;
	this->_count = count;
	if (serialNumber.size() != 5 || unitPrice <= 0)
		throw("ERROR: problem with the item's params");
	else
	{
		this->_unitPrice = unitPrice;
		this->_serialNumber = serialNumber;
	}
	
}
Item::~Item()
{

}

double Item::totalPrice() const
{
	return this->_count* this->_unitPrice;
}
bool Item::operator <(const Item& other) const
{
	return this->_serialNumber < other._serialNumber;
}
bool Item::operator >(const Item& other) const
{
	return this->_serialNumber > other._serialNumber;
}
bool Item::operator ==(const Item& other) const
{
	return this->_serialNumber == other._serialNumber;
}
double Item::getUnitPrice() const
{
	return this->_unitPrice;
}
std::string Item::getName() const
{
	return this->_name;
}

int Item::getCount()const
{
	return this->_count;
}


std::string Item::getSerialNumber() const
{
	return this->_serialNumber;
}