#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(std::string name);
	Customer();
	double totalSum() const; //returns the total sum for payment
	void addItem(Item item); //add item to the set
	void removeItem(Item item); //remove item from the set
	std::set<Item> getItems()const;
	std::string getName() const;
	//get and set functions

private:
	std::string _name;
	std::set<Item> _items;


};
