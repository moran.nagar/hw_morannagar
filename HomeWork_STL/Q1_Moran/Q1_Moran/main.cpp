#include"Customer.h"
#include<map>

using namespace std;
void print_all_items(std::set<Item> s);
int main()
{

	std::map<std::string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3, 1),
		Item("Cookies","00002",12.6, 1),
		Item("bread","00003",8.9, 1),
		Item("chocolate","00004",7.0, 1),
		Item("cheese","00005",15.3, 1),
		Item("rice","00006",6.2, 1),
		Item("fish", "00008", 31.65, 1),
		Item("chicken","00007",25.99, 1),
		Item("cucumber","00009",1.21, 1),
		Item("tomato","00010",2.32, 1)};
	cout << "Welcome to MagshiMart!" << endl;
	int choice = 0;
	while (choice != 4)
	{
		cout << "1. To sign as customer and but items" << endl;
		cout << "2. To update existing customer's item" << endl;
		cout << "3. To print the customer who pays the most" << endl;
		cout << "4. To exit." << endl;
		cin >> choice;
		switch (choice)
		{
			case 1:
			{
				string name = "";
				cout << "Enter name: ";
				cin >> name;
				if (abcCustomers.find(name) != abcCustomers.end())
				{
					cout << "ERROR: THE NAME ALREADY EXISTS" << endl;
					break;
				}
				std::set<Item> s1{ std::begin(itemList), std::end(itemList) };
				print_all_items(s1);

				Customer* c = new Customer(name);
				cout << "What item would you like to buy?";
				int itemNum = 0;
				cin >> itemNum;
				while (itemNum != 0)
				{
					c->addItem(itemList[itemNum]);
					cin >> itemNum;
				}
				abcCustomers[name] = *c;
				break;
			}

			case 2:
			{
				string name = "";
				cout << "Enter name: ";
				cin >> name;
				if (abcCustomers.find(name) == abcCustomers.end())
				{
					cout << "ERROR: THIS NAME DOES'NT EXIST" << endl;
					break;
				}
				print_all_items(abcCustomers[name].getItems());
				 
				cout << "1. Add items.\n2. Remove items.\n3. Back to menu.\n"<<endl;
				int user_choice = 0;
				cin >> user_choice;
				switch (user_choice)
				{
				case 1:
				{
					cout << "What item would you like to buy?";
					int itemNum = 0;
					cin >> itemNum;
					while (itemNum != 0)
					{
						abcCustomers[name].addItem(itemList[itemNum]);
						cin >> itemNum;
					}
					break;
				}
				case 2:
				{
					cout << "Which item would you like to remove from the list?";
					int itemNum = 0;
					cin >> itemNum;
					while (itemNum != 0)
					{
						abcCustomers[name].removeItem(itemList[itemNum]);
						cin >> itemNum;
					}
					break;
				}
				case 3:
				{
					break;
				}
				default:
					cout << "ERROR: This choice doesn't exist" << endl;
					break;
				}
				break;
			}
			case 3:
			{
				string name = "";
				int max = 0;
				int currtotal = 0;
				map<string, Customer>::iterator it;
				for (it = abcCustomers.begin(); it != abcCustomers.end(); ++it)
				{
					currtotal = (*it).second.totalSum();
					if (max < currtotal)
					{
						max = currtotal;
						name = (*it).second.getName();
					}

				}
				cout << "Max price is " << max << ", name: " << name << endl;
				break;
			}
			case 4:
			{ 
				cout << "Goodbye!" << endl;
				break;
			}
			default:
				cout << "ERROR: This choice doesn't exist" << endl;
				break;
		}
	}
	
	
	return 0;

}

void print_all_items(std::set<Item> s)
{
	int count = 1;
	for (std::set<Item>::iterator it = s.begin(); it != s.end(); ++it)
	{
		cout << count << ". " << (*it).getName() << " price: " << (*it).getUnitPrice() << ", count:"<< (*it).getCount() << endl;
		count++;
	}
}







