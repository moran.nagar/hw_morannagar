#include "Customer.h"
#include<string>
#include<algorithm>

using namespace std;

Customer::Customer(std::string name)
{
	this->_name = name;
}
Customer::Customer()
{

}
double Customer::totalSum() const
{
	double sum = 0;
	set <Item> ::iterator itr;
	for (itr = this->_items.begin(); itr != this->_items.end(); ++itr)
	{
		sum += (*itr).totalPrice();
	}
	return sum;
}
void Customer::addItem(Item item)
{
	set<Item>::iterator itr;
	for (itr = this->_items.begin(); itr != this->_items.end(); ++itr)
	{
		if (item == *itr)
		{
			int count = (*itr).getCount();
			this->_items.erase(*itr);
			this->_items.insert(Item(item.getName(), item.getSerialNumber(), item.getUnitPrice(), (count + 1)));
			return;
		}
	}
	this->_items.insert(item);
}
void Customer::removeItem(Item item)
{
	set<Item>::iterator itr;
	for (itr = this->_items.begin(); itr != this->_items.end(); ++itr)
	{
		if (item == (*itr) && (*itr).getCount() > 1)
		{
			int count = (*itr).getCount();
			this->_items.erase(*itr);
			this->_items.insert(Item(item.getName(), item.getSerialNumber(), item.getUnitPrice(), (count - 1)));
			return;
		}
	}
	this->_items.erase(item);
}

std::set<Item> Customer::getItems()const
{
	return this->_items;
}
std::string Customer::getName() const
{
	return this->_name;
}