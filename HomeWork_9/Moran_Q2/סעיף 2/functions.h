#pragma once
#include <iostream>

template < class T >
int compare(T paramOne, T paramTwo)
{
	if (paramOne < paramTwo)
		return 1;
	if (paramOne == paramTwo)
		return 0;
	return -1;
}

template < class T >
void bubbleSort(T a[], int n)
{
	int i, j;
	T temp;
	for (i = 0; i < n; i++)
	{
		for (j = i + 1; j < n; j++)
		{
			if (a[i] > a[j])
			{
				temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
	}
}

template < class T > 
void printArray(const T a[], int n)
{
	for (int i = 0; i < n; ++i) 
		std::cout << a[i] << ' ';
	std::cout << '\n';
}
