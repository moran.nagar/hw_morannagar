#include "BSNode.h"
#include <algorithm>
#include <iostream>
#include <string.h>
using namespace std;


BSNode::BSNode(std::string data)
{
	this->_data = data;
	this->_left = NULL;
	this->_right = NULL;
	this->_count = 1;
}

//I didn't succeed
BSNode::BSNode(const BSNode& other)
{
	
}

BSNode::~BSNode()
{
	if (!this)
		return;
	BSNode* curBSNode = this;
	BSNode* leftBSNode = this->_left;
	BSNode* rightBSNode = this->_right;
	delete(curBSNode);
	leftBSNode->~BSNode();
	rightBSNode->~BSNode();
}

void BSNode::insert(std::string value)
{
	this->insert(this, value);
}
BSNode* BSNode::insert(BSNode* root, std::string value)
{
	if (!root)
		root = new BSNode(value);
	else if (root->_data == value)
		this->_count++;
	else if (value < root->_data)
		root->_left = insert(root->_left, value);
	else if (value > root->_data)
		root->_right = insert(root->_right, value);
	return root;
}

//I didn't succeed
BSNode& BSNode::operator=(const BSNode& other)
{
	BSNode temp = other;
	return temp;
}

bool BSNode::isLeaf() const
{
	return this->_left == NULL && this->_right == NULL;
}
std::string BSNode::getData() const
{
	return this->_data;
}

BSNode* BSNode::getLeft() const
{
	return this->_left;
}
BSNode* BSNode::getRight() const
{
	return this->_right;
}
bool BSNode::search(std::string val)const
{
	if (!this)
		return false;
	if (this->_data == val)
		return true;
	if (this->_right->_data <= val)
		return this->_right->search(val);
	return this->_left->search(val);
}

int BSNode::getHeight() const
{
	if (this->isLeaf())
		return 1;
	if (this->_left == NULL && this->_right != NULL)
		return 1 + this->_right->getHeight();
	if (this->_right == NULL && this->_left != NULL)
		return 1 + this->_left->getHeight();
	return 1 + std::max(this->_left->getHeight(), this->_right->getHeight());
}

//I didn't succeed
int BSNode::getDepth(const BSNode& root) const
{
	if (!this)
	{
		return -1;
	}
	if (this->_data == root._data)
	{
		return 1;
	}
	if (this->_data < root._data)
	{
		return 1 + this->_right->getDepth(root);
	}
	if (this->_data > root._data)
	{
		return 1 + this->_left->getDepth(root);
	}
	return -1;
	
}
void BSNode::printNodes() const
{
	if (!this)
		return;
	this->_left->printNodes();
	cout << this->_data << " -> "<< this->_count<< endl;
	this->_right->printNodes();
}


