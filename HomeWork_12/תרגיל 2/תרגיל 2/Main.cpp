#include "MessagesSender.h"
#include <thread>
using namespace std;

int main()
{
	MessagesSender* admin = new MessagesSender();
	admin->menu();
	thread t1(&MessagesSender::check_the_data, admin);
	thread t2(&MessagesSender::send, admin);
	t1.join();
	t2.join();

	

	return 0;
}