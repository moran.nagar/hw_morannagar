#include "MessagesSender.h"
#include <fstream>
#include <string>
#include <chrono>
#include <thread>
#include <mutex>
#include <Windows.h>


using namespace std;
mutex m;
condition_variable cond;

void MessagesSender::menu()
{
	print_menu();
	int choice = 0;
	cout << "Enter your choice: \n";
	cin >> choice;
	switch (choice)
	{
	case 1:
		Signin();
		break;
	case 2:
		Signout();
		break;
	case 3:
		print_connected_users();
		break;
	case 4:
		break;
	default:
		cout << "Invalid choice\n";
		break;
	}
}

void MessagesSender::print_menu()
{
	cout << "1. Signin\n2. Signout\n3. Connected Users\n4. exit\n";
}

void MessagesSender::Signin()
{
	string name = "";
	cout << "Enter username: \n";
	cin >> name;
	for (int i = 0; i < this->users.size(); i++)
	{
		if (name == this->users[i])
		{
			cout << "You are already connected\n";
			return;
		}
	}
	this->users.push_back(name);
}

void MessagesSender::Signout()
{
	string name = "";
	cout << "Enter username: \n";
	cin >> name;
	vector<string>::iterator it;
	for (it = this->users.begin(); it < this->users.end(); it++)
	{
		if (name == (*it))
		{
			cout << "Goodbye\n";
			this->users.erase(it);
			return;
		}
	}
	cout << "Your username isn't connected\n";
}

void MessagesSender::print_connected_users()
{
	vector<string>::iterator it;
	for (it = this->users.begin(); it < this->users.end(); it++)
	{
		cout << ("username: %s\n",*it);
	}
}



void MessagesSender::check_the_data()
{
	string line;
	ifstream data_file("data.txt");
	while (data_file.is_open())
	{
		if (!(data_file.peek() == ifstream::traits_type::eof()))
		{
			m.lock();
			while (getline(data_file, line))
			{
				this->messages.push_back(line);
			}
			data_file.clear();
			m.unlock();
			cond.notify_one();
		}
		data_file.close();
		Sleep(60000);
	}
	
}

void MessagesSender::send()
{
	ofstream output_file("output.txt");
	if (output_file.is_open())
	{
		unique_lock<mutex> locker(m);
		if (this->messages.empty())
		{
			cond.wait(locker);
		}
		for (vector<string>::iterator it = this->messages.begin(); it < this->messages.end(); it++)
		{
			for (string name : this->users)
			{
				output_file << ("%s: %s, ", (name, (*it)));
			}
			this->messages.erase(it);
			output_file << "\n";
		}
		m.unlock();
		output_file.close();
	}
	else
		cout << "Unable to open file";
}