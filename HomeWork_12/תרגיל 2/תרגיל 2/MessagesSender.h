#pragma once
#include <iostream>
#include <vector>

using namespace std;

class MessagesSender
{
public:
	void menu();
	void check_the_data();
	void send();
private:

	void print_connected_users();
	void Signout();
	void Signin();
	void print_menu();
	vector<string> users;
	vector<string> messages;
};
