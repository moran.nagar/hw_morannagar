#include "threads.h"
#include <iterator>
#include <thread>
#include <time.h>
#include <mutex>

using namespace std;
std::mutex m;

void I_Love_Threads()
{
	cout << "I Love Threads";
}

void call_I_Love_Threads()
{
	thread t(I_Love_Threads);
	t.join();
}

void printVector(std::vector<int> primes)
{
	for (int i = 0; i < primes.size(); i++)
	{
		cout << primes[i] << endl;
	}
}

void getPrimes(int begin, int end, std::vector<int>& primes)
{
	bool isPrime = true;
	for (int n = begin; n <= end; n++)
	{
		for (int i = 2; i <= n / 2; ++i)
		{
			if (n % i == 0)
			{
				isPrime = false;
				break;
			}
		}
		if (isPrime)
		{
			primes.push_back(n);
		}
		isPrime = true;
	}
	
}
std::vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;
	clock_t time;
	time = clock();
	thread thread(getPrimes, begin, end, ref(primes));
	thread.join();
	time = clock() - time;
	cout << (float)time << endl;
	return primes;
}


void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	bool isPrime = true;
	for (int n = begin; n <= end; n++)
	{
		for (int i = 2; i <= n / 2; ++i)
		{
			if (n % i == 0)
			{
				isPrime = false;
				break;
			}
		}
		if (isPrime)
		{
			file << n << endl;
		}
		isPrime = true;
	}
}
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	pair <int, int> range;
	int size_of_thread = (end - begin) / N;
	range.first = begin;
	range.second = end;
	ofstream file;
	file.open(filePath, ios::out | ios::in);
	if (file.is_open())
	{
		clock_t time;
		time = clock();
		
		for (int i = 1; i <= N; i++)
		{
			m.lock();
			thread thread(writePrimesToFile, range.first, range.second, ref(file));
			m.unlock();
			thread.join();
			range.first = range.second + 1;
			range.second = range.second + size_of_thread;
		}
		
		time = clock() - time;
		cout << (float)time << endl;
	}
	
}
